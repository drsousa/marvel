//
//  MockTwitchAPI.swift
//  TopGamesPANTests
//
//  Created by Daniel Rocha on 3/30/18.
//  Copyright © 2018 Daniel Sousa. All rights reserved.
//

import Foundation
@testable import Marvel

class MockAbstractNetworking: Networking {
    
    
    override func doGet<P, R>(requestObject: P, success: @escaping (R?) -> ()?, failure: @escaping (NetworkingError?) -> ()) where P : AbstractRequest, R : ResponseProtocol {

        var characters = CharactersResponse()
        let results = ResultCharactersResponse()
        results.id = 1009146
        results.name = "Abomination (Emil Blonsky)"
        characters.results = [results]
        
        if let response = characters as? R {
            success(response)
        }
        
    }
    
    
}
