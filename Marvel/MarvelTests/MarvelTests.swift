//
//  MarvelTests.swift
//  MarvelTests
//
//  Created by Daniel Sousa on 20/04/18.
//  Copyright © 2018 Daniel Sousa. All rights reserved.
//

import XCTest
@testable import Marvel

class MarvelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    /**
     Testar se o mapper esta sendo feito de forma correta
     */
    func testMapperCharacter() {

        let mock: MockAbstractNetworking = MockAbstractNetworking()
        let api: CharactersAPI = CharactersAPI(networking: mock)
        
        api.getCharacters(offset: 1, success: { (response) -> Optional<()> in
            
            guard let results = response?.results else {
                XCTFail("Deve retornar um response valido")
                return ()
            }
            
            XCTAssertEqual(results[0].id , 1009146)
            XCTAssertEqual(results[0].name , "Abomination (Emil Blonsky)")
            return ()
            
        }, failure: { (error) in
            XCTFail("Deve retornar um response valido")
            return ()
        })

    }
    
}
