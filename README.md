# Marvel

- Para buildar o projeto sera necessário rodar o comando 'pod install' no terminal (Lembrar de acessar a pasta Marvel/Marvel).
- Foi feito um teste unitário e um teste de interface, eles estao nas pastas MarvelTests e MarvelUITests.

- Utilizei a biblioteca AlamofireImage pela sua facilidade e efeitos visuais que já estao prontos
- Utilizei o Xcode 10.1 Swift 4.2

Obrigado
